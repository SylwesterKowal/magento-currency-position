<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CurrencyPosition\Observer\Frontend\Currency;

class DisplayOptionsForming implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Kowal\DisableAddToCart\Helper\Data $helperData
    )
    {
        $this->helperData = $helperData;

    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $this->enable = $this->helperData->getGeneralCfg("enable");
        $this->position = $this->helperData->getGeneralCfg("position");
        if( $this->enable && $this->position ){
            $currencyOptions = $observer->getEvent()->getCurrencyOptions();
            if($this->position == 'left') $currencyOptions->setData('position', \Magento\Framework\Currency::LEFT);
            if($this->position == 'right') $currencyOptions->setData('position', \Magento\Framework\Currency::RIGHT);
        }

        return $this;
    }
}

